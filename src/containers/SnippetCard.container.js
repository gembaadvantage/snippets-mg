import { useCallback, useEffect, useState } from "react";
import { useFetch } from "../api/useFetch.hook";
import SnippetCard from "../components/SnippetCard";

function SnippetCardContainer({ snippetId }) {
  const [makeRequest, { data }] = useFetch();

  const [endpoint] = useState(
    `/snippets/${snippetId}/?_expand=author&_expand=language`,
  );

  const fetchSnippet = useCallback(
    () => makeRequest(endpoint),
    [makeRequest, endpoint],
  );

  useEffect(fetchSnippet, [fetchSnippet]);

  if (data) {
    return <SnippetCard {...data} key={data.id} />;
  }

  return null;
}

export default SnippetCardContainer;
