// Simple icon text button with Mono icons
// https://icons.mono.company/

import style from "./IconTextButton.module.css";

const IconTextButton = ({ label, icon, onClick, disabled, ...props }) => {
  return (
    <button
      disabled={disabled}
      className={style.Button}
      onClick={onClick}
      {...props}
    >
      {label} <i className={`mi-${icon}`} />
    </button>
  );
};

export default IconTextButton;
