import "@material/typography/dist/mdc.typography.css";
import "mono-icons/iconfont/icons.css";
import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import "./index.css";
import "./theme.css";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
