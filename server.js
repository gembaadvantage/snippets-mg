const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("db/db.json");
const middlewares = jsonServer.defaults();

// Optionally simulate a slow response
const delayMs = 0;

server.use(middlewares);
server.use((req, res, next) => {
  setTimeout(next, delayMs);
});
server.use(router);
server.listen(8080, () => {
  console.log("Server is running on 8080");
});
